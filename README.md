# GreenActing

## Projektbeschreibung
Unser Projekt ist eine Website, die einem Tipps gibt wie man nachhaltiger Leben kann und die einem Veranstaltungen zum Thema Umwelt zeigt zum Beispiel demonstationen oder ein Gruppen-treffen

## Github- / Gitlab- oder ggf. Projektlink

* GitLab: https://gitlab.com/jugendhackt/greenacting

## Name der Teilnehmer\*innen

* Maya
* Jane
* Tete
* Dilay
* Thea
* Daria

## Name der begleitenden Mentor\*innen

* MatzE
* Markus
* Johanna G

## Logo des Prototypens oder ggf. Foto

* hier könnt ihr euer Logo und/oder weitere Bilder vom Projekt zeigen (Denkt bitte dabei an das Recht am eigenen Bild und natürlich auch das Urheberrecht)
* solltet ihr nicht wissen, wie man hier ein Bild einfügt, schaut gern in dieser Hilfedatei nach: https://pad.medialepfade.net/howtohackmd



Hilfe allgemein: https://pad.medialepfade.net/howtohackmd
